import Foundation

struct NowPlayingResponse {
    let movies: [Movie]
}

extension NowPlayingResponse {
    init?(json: Any) {
        guard let jsonObject = json as? [String: Any] else {
            return nil
        }

        guard let moviesJSON = jsonObject["results"] as? [[String: Any]] else {
            return nil
        }

        let moviesByMappingFromJSON = moviesJSON.map { try? Movie(json: $0) }
        let validMovies = moviesByMappingFromJSON.flatMap { $0 }

        self.init(movies: validMovies)
    }
}

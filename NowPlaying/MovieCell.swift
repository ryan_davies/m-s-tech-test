import UIKit

class MovieCell: UICollectionViewCell {
    private static var titleFont: UIFont {
        return .preferredFont(forTextStyle: .headline)
    }

    private static let titleLabelNumberOfLines = 2
    private static let horizontalSpacing: CGFloat = 8
    private static let verticalSpacing: CGFloat = 8

    private let imageView: UIImageView = {
        let view = UIImageView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = MovieCell.titleFont
        label.numberOfLines = MovieCell.titleLabelNumberOfLines
        label.textColor = .white
        label.textAlignment = .center
        return label
    }()

    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupSubviews()
    }

    private func setupSubviews() {
        addViewsToContentView()
        setupConstraints()
    }

    private func addViewsToContentView() {
        contentView.addSubview(imageView)
        contentView.addSubview(titleLabel)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate([
            imageView.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: MovieCell.horizontalSpacing),
            imageView.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -MovieCell.horizontalSpacing),
            imageView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: MovieCell.verticalSpacing),
            imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1.5),

            titleLabel.leftAnchor.constraint(equalTo: contentView.leftAnchor, constant: MovieCell.horizontalSpacing),
            titleLabel.rightAnchor.constraint(equalTo: contentView.rightAnchor, constant: -MovieCell.horizontalSpacing),

            titleLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: MovieCell.verticalSpacing)
        ])
    }

    static func size(width: CGFloat) -> CGSize {
        return CGSize(width: width,
                      height: height(width: width))
    }

    static func height(width: CGFloat) -> CGFloat {
        let spacingAtTop = verticalSpacing
        let spacingAtBottom = verticalSpacing
        let spacingBetweenImageAndLabel = verticalSpacing
        let totalSpacing = [spacingAtTop, spacingAtBottom, spacingBetweenImageAndLabel].reduce(0, +)

        let imageAspectRatio = 1.5
        let aspectFitImageHeight = imageWidth(in: width) * CGFloat(imageAspectRatio)

        return (aspectFitImageHeight + totalSpacing + titleLabelHeight).rounded(.up)
    }

    static func imageWidth(in width: CGFloat) -> CGFloat {
        return width - (horizontalSpacing * 2)
    }

    static private var titleLabelHeight: CGFloat {
        return titleFont.lineHeight * CGFloat(titleLabelNumberOfLines)
    }
}


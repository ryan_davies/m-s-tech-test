import Foundation

struct MovieViewModel {
    let movie: Movie

    var title: String {
        return movie.title ?? ""
    }

    var releaseYear: String {
        guard let releaseDate = movie.releaseDate else {
            return "Unknown"
        }

        let calendar = Calendar.current
        let year = calendar.component(.year, from: releaseDate)
        return "(\(year))"
    }

    var overview: String {
        return movie.overview ?? ""
    }

    func imageURL(for width: Int) -> URL? {
        guard let path = movie.posterImage?.path else {
            return nil
        }

        let supportedImageWidths = [
            92,
            154,
            185,
            342,
            500,
            780
        ]

        let closestImageWidth = supportedImageWidths.min { (left: Int, right: Int) -> Bool in
            return abs(left - width) < abs(right - width)
        }

        guard let imageWidth = closestImageWidth else {
            return nil
        }

        return URL(string: "https://image.tmdb.org/t/p/w\(imageWidth)\(path)")
    }
}

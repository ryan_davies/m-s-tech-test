import UIKit

class ImageService {
    let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    typealias FetchCompletion = (UIImage?) -> Void

    @discardableResult
    func fetchImage(url: URL, completionHandler: @escaping FetchCompletion) -> URLSessionTask? {
        let task = session.dataTask(with: url) { (data: Data?, _: URLResponse?, _: Error?) in
            guard let data = data else {
                DispatchQueue.main.async {
                    completionHandler(nil)
                }
                return
            }

            guard let image = UIImage(data: data) else {
                DispatchQueue.main.async {
                    completionHandler(nil)
                }
                return
            }

            DispatchQueue.main.async {
                completionHandler(image)
            }
        }

        task.resume()
        return task
    }
}

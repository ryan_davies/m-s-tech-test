import Foundation

class NowPlayingService {
    let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    typealias FetchCompletion = (NowPlayingResponse?) -> Void

    @discardableResult
    func fetchNowPlaying(completionHandler: @escaping FetchCompletion) -> URLSessionTask? {
        func response(data: Data?) -> NowPlayingResponse? {
            guard let data = data else {
                return nil
            }

            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                return nil
            }

            guard let response = NowPlayingResponse(json: json) else {
                return nil
            }
            
            return response
        }

        guard let url = url else {
            DispatchQueue.main.async {
                completionHandler(nil)
            }
            return nil
        }

        let task = session.dataTask(with: url) { (data: Data?, _: URLResponse?, _: Error?) in
            guard let response = response(data: data) else {
                DispatchQueue.main.async {
                    completionHandler(nil)
                }
                return
            }

            DispatchQueue.main.async {
                completionHandler(response)
            }
        }
        
        task.resume()
        return task
    }

    private var url: URL? {
        guard var urlComponents = URLComponents(string: "https://api.themoviedb.org/3/movie/now_playing") else {
            return nil
        }

        let apiKeyQueryItem = ServiceConfiguration.apiKeyURLQueryItem
        let languageQueryItem = URLQueryItem(name: "language", value: "en-GB")
        let pageQueryItem = URLQueryItem(name: "page", value: "1")
        let regionQueryItem = URLQueryItem(name: "region", value: "GB")
        urlComponents.queryItems = [apiKeyQueryItem, languageQueryItem, pageQueryItem, regionQueryItem]

        guard let url = urlComponents.url else {
            return nil
        }

        return url
    }
}

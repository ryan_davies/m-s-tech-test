import UIKit

class MovieDetailView: UIView {
    init(frame: CGRect,
         collectionViewDataSource: UICollectionViewDataSource?,
         collectionViewDelegate: UICollectionViewDelegate?) {

        super.init(frame: frame)
        setBackgroundColor()
        setupSubviews(
            collectionViewDataSource: collectionViewDataSource,
            collectionViewDelegate: collectionViewDelegate
        )
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setBackgroundColor()
        setupSubviews(collectionViewDataSource: nil,
                      collectionViewDelegate: nil)
    }

    private let imageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    var image: UIImage? {
        get {
            return imageView.image
        }
        set {
            imageView.image = newValue
        }
    }

    private var titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Hello"
        label.font = .preferredFont(forTextStyle: .headline)
        label.textColor = .white
        return label
    }()

    var title: String? {
        get {
            return titleLabel.text
        }
        set {
            titleLabel.text = newValue
        }
    }

    private var releaseYearLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Hello"
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .white
        return label
    }()

    var releaseYear: String? {
        get {
            return releaseYearLabel.text
        }
        set {
            releaseYearLabel.text = newValue
        }
    }

    private var overviewLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Hello"
        label.numberOfLines = 0
        label.font = .preferredFont(forTextStyle: .body)
        label.textColor = .white
        return label
    }()

    var overview: String? {
        get {
            return overviewLabel.text
        }
        set {
            overviewLabel.text = newValue
        }
    }

    private var collectionHeadingLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = .preferredFont(forTextStyle: .subheadline)
        label.textColor = .white
        label.text = "More in this collection"
        return label
    }()

    private var collectionViewHeightConstraint: NSLayoutConstraint?

    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal

        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .darkGray
        collectionView.showsHorizontalScrollIndicator = false

        return collectionView
    }()

    private func setBackgroundColor() {
        backgroundColor = .darkGray
    }

    private func setupImageView() {
        addSubview(imageView)

        NSLayoutConstraint.activate([
            imageView.leftAnchor.constraint(equalTo: leftAnchor),
            imageView.rightAnchor.constraint(equalTo: rightAnchor),
            imageView.topAnchor.constraint(equalTo: topAnchor),
            imageView.heightAnchor.constraint(equalTo: widthAnchor)
        ])
    }

    private func setupStackView() {
        let subviews: [UIView] = [
            titleLabel,
            releaseYearLabel,
            overviewLabel,
            collectionHeadingLabel,
            collectionView
        ]

        let stackView = UIStackView(arrangedSubviews: subviews)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.spacing = 8

        addSubview(stackView)

        NSLayoutConstraint.activate([
            stackView.leftAnchor.constraint(equalTo: leftAnchor, constant: 8),
            stackView.rightAnchor.constraint(equalTo: rightAnchor, constant: -8),
            stackView.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8),
        ])
    }

    private func setupSubviews(collectionViewDataSource: UICollectionViewDataSource?,
                               collectionViewDelegate: UICollectionViewDelegate?) {
        setupImageView()
        setupStackView()
        addCollectionViewHeightConstraint()
        showOrHideCollectionViewAndHeadingLabel(collectionViewHidden: true)

        collectionView.dataSource = collectionViewDataSource
        collectionView.delegate = collectionViewDelegate
    }

    private func addCollectionViewHeightConstraint() {
        let heightConstraint = collectionView.heightAnchor.constraint(equalToConstant: 0)
        collectionView.addConstraint(heightConstraint)
        collectionViewHeightConstraint = heightConstraint
    }

    private func updateCollectionViewHeightConstraint() {
        collectionViewHeightConstraint?.constant = MovieCell.height(width: collectionViewCellSize.width)
    }

    var collectionViewCellSize: CGSize {
        let numberOfVisibleCells: CGFloat = 2.5
        let cellWidth = bounds.size.width / numberOfVisibleCells
        return CGSize(width: cellWidth,
                      height: MovieCell.height(width: cellWidth))
    }

    func showOrHideCollectionViewAndHeadingLabel(collectionViewHidden: Bool) {
        [collectionHeadingLabel, collectionView].forEach { view in
            view.isHidden = collectionViewHidden
        }
    }

    override func layoutSubviews() {
        updateCollectionViewHeightConstraint()
        super.layoutSubviews()
    }
}

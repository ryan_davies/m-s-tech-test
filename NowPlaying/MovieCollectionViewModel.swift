import Foundation

struct MovieCollectionViewModel {
    let collection: MovieCollection

    var numberOfParts: Int {
        return collection.parts.count
    }

    func part(indexPath: IndexPath) -> MovieViewModel? {
        let index = indexPath.item
        guard collection.parts.indices.contains(index) else {
            return nil
        }
        return MovieViewModel(movie: collection.parts[index])
    }
}

import Foundation

struct Movie {
    let identifier: Int?
    let title: String?
    let releaseDate: Date?
    let overview: String?
    let posterImage: Image?
    let collection: MovieCollection?
}

extension Movie {
    struct Image {
        let path: String
    }
}

extension Movie {
    enum Error: Swift.Error {
        case invalidRootObject
    }

    init(json: Any) throws {
        guard let jsonObject = json as? [String: Any] else {
            throw Error.invalidRootObject
        }

        let identifier = jsonObject["id"] as? Int
        let title = jsonObject["title"] as? String
        let releaseDate = Movie.releaseDateFrom(string: jsonObject["release_date"] as? String)
        let overview = jsonObject["overview"] as? String
        let posterPath = jsonObject["poster_path"] as? String

        let posterImage: Image? = {
            if let path = posterPath {
                return Image(path: path)
            }
            return nil
        }()

        let collection: MovieCollection? = {
            if let belongsToCollection = jsonObject["belongs_to_collection"] {
                return MovieCollection(json: belongsToCollection)
            }
            return nil
        }()

        self.init(identifier: identifier,
                  title: title,
                  releaseDate: releaseDate,
                  overview: overview,
                  posterImage: posterImage,
                  collection: collection)
    }

    private static func releaseDateFrom(string releaseDate: String?) -> Date? {
        if let releaseDateString = releaseDate {
            return Movie.releaseDateFrom(string: releaseDateString)
        }
        return nil
    }

    private static func releaseDateFrom(string releaseDate: String) -> Date? {
        return Movie.dateFormatter.date(from: releaseDate)
    }

    private static var dateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        return dateFormatter
    }()
}

import Foundation

class MovieCollectionService {
    let session: URLSession

    init(session: URLSession) {
        self.session = session
    }

    typealias FetchCompletion = (MovieCollection?) -> Void

    @discardableResult
    func fetchMovieCollection(id: Int, completionHandler: @escaping FetchCompletion) -> URLSessionTask? {
        func response(data: Data?) -> MovieCollection? {
            guard let data = data else {
                return nil
            }

            guard let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                return nil
            }

            guard let response = MovieCollection(json: json) else {
                return nil
            }

            return response
        }

        guard let url = urlForCollection(identifier: id) else {
            DispatchQueue.main.async {
                completionHandler(nil)
            }
            return nil
        }

        let task = session.dataTask(with: url) { (data: Data?, _: URLResponse?, _: Error?) in
            guard let response = response(data: data) else {
                DispatchQueue.main.async {
                    completionHandler(nil)
                }
                return
            }

            DispatchQueue.main.async {
                completionHandler(response)
            }
        }

        task.resume()
        return task
    }

    private func urlForCollection(identifier: Int) -> URL? {
        guard var urlComponents = URLComponents(string: "https://api.themoviedb.org/3/collection/") else {
            return nil
        }

        urlComponents.path = urlComponents.path.appending(String(identifier))

        let apiKeyQueryItem = ServiceConfiguration.apiKeyURLQueryItem
        let languageQueryItem = URLQueryItem(name: "language", value: "en-GB")
        urlComponents.queryItems = [apiKeyQueryItem, languageQueryItem]

        guard let url = urlComponents.url else {
            return nil
        }
        
        return url
    }
}

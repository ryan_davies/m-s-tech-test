import UIKit

class MoviesViewController: UICollectionViewController {
    private static let cellIdentifier = "Movie"
    private let service = NowPlayingService(session: URLSession.shared)
    private let imageService = ImageService(session: URLSession.shared)
    private var movieViewModels: [MovieViewModel] = []

    init() {
        super.init(collectionViewLayout: MoviesViewController.defaultLayout)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override var navigationItem: UINavigationItem {
        let item = super.navigationItem
        item.title = "Now Playing"
        return item
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        fetchNowPlaying()
    }

    private func fetchNowPlaying() {
        guard movieViewModels.isEmpty else {
            return
        }

        service.fetchNowPlaying { [weak self] (response: NowPlayingResponse?) in
            self?.movieViewModels = response?.movies.map({ MovieViewModel(movie: $0) }) ?? []
            self?.collectionView?.reloadData()
        }
    }

    private static var defaultLayout: UICollectionViewLayout {
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        return flowLayout
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView?.backgroundColor = .darkGray
        collectionView?.alwaysBounceVertical = true
        collectionView?.register(MovieCell.self, forCellWithReuseIdentifier: MoviesViewController.cellIdentifier)
    }

    func movieViewModel(at indexPath: IndexPath) -> MovieViewModel? {
        let index = indexPath.item
        guard movieViewModels.indices.contains(index) else {
            return nil
        }
        return movieViewModels[index]
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieViewModels.count
    }

    override func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MoviesViewController.cellIdentifier,
                                                      for: indexPath)

        if let movieCell = cell as? MovieCell, let movieViewModel = self.movieViewModel(at: indexPath) {
            movieCell.title = movieViewModel.title
            movieCell.image = nil

            fetchImage(
                movieViewModel: movieViewModel,
                forCell: movieCell,
                indexPath: indexPath,
                collectionView: collectionView
            )
        }
        return cell
    }

    private func fetchImage(movieViewModel: MovieViewModel,
                            forCell cell: MovieCell,
                            indexPath: IndexPath,
                            collectionView: UICollectionView) {

        let cellSize = self.collectionView(collectionView,
                                           layout: collectionView.collectionViewLayout,
                                           sizeForItemAt: indexPath)
        let imageWidthAsFloat = MovieCell.imageWidth(in: cellSize.width)
        let imageWidthByApplyingScale = imageWidthAsFloat * UIScreen.main.scale
        let imageWidth = Int(imageWidthByApplyingScale.rounded(.up))

        guard let imageURL = movieViewModel.imageURL(for: imageWidth) else {
            return
        }

        imageService.fetchImage(url: imageURL) { (image: UIImage?) in
            guard let image = image else {
                return
            }

            cell.image = image
        }
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedMovieViewModel = movieViewModel(at: indexPath) else {
            return
        }

        guard let selectedMovieID = selectedMovieViewModel.movie.identifier else {
            return
        }

        let detailViewController = MovieDetailViewController(movieID: selectedMovieID)
        show(detailViewController, sender: nil)
    }
}

extension MoviesViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        let halfWidth = collectionView.bounds.size.width * 0.5
        return MovieCell.size(width: halfWidth)
    }
}

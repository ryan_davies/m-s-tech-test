import Foundation

struct ServiceConfiguration {
    static let apiKey = "865e987e4dca793b4378cab72d4c45fe"
    static let apiKeyURLQueryItem = URLQueryItem(name: "api_key", value: apiKey)
}

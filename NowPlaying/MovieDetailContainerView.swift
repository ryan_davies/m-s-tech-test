import UIKit

class MovieDetailContainerView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setBackgroundColor()
        addSubviews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setBackgroundColor()
        addSubviews()
    }

    private func setBackgroundColor() {
        backgroundColor = .darkGray
    }

    private func addSubviews() {
        addScrollView()
        showActivityIndicator()
    }

    private let scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.alwaysBounceVertical = true
        return scrollView
    }()

    private func addScrollView() {
        addSubview(scrollView)

        NSLayoutConstraint.activate([
            scrollView.leftAnchor.constraint(equalTo: leftAnchor),
            scrollView.rightAnchor.constraint(equalTo: rightAnchor),
            scrollView.topAnchor.constraint(equalTo: topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    var detailView: MovieDetailView? {
        didSet {
            if let oldDetailView = oldValue {
                oldDetailView.removeFromSuperview()
            }

            if let newDetailView = detailView {
                add(detailView: newDetailView, to: scrollView)
                hideActivityIndicator()
            }
        }
    }

    private func add(detailView: UIView, to parent: UIView) {
        detailView.translatesAutoresizingMaskIntoConstraints = false
        parent.addSubview(detailView)

        NSLayoutConstraint.activate([
            detailView.widthAnchor.constraint(equalTo: parent.widthAnchor),
            detailView.leftAnchor.constraint(equalTo: parent.leftAnchor),
            detailView.rightAnchor.constraint(equalTo: parent.rightAnchor),
            detailView.topAnchor.constraint(equalTo: parent.topAnchor),
            detailView.bottomAnchor.constraint(equalTo: parent.bottomAnchor)
        ])
    }

    private weak var activityIndicator: UIActivityIndicatorView?

    private func showActivityIndicator() {
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.translatesAutoresizingMaskIntoConstraints = false
        addSubview(indicator)

        NSLayoutConstraint.activate([
            indicator.centerXAnchor.constraint(equalTo: centerXAnchor),
            indicator.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])

        indicator.startAnimating()
        activityIndicator = indicator
    }

    private func hideActivityIndicator() {
        guard let indicator = activityIndicator else {
            return
        }

        indicator.removeFromSuperview()
    }
}

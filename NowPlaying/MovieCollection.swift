import Foundation

struct MovieCollection {
    let identifier: Int?
    let title: String?
    let parts: [Movie]
}

extension MovieCollection {
    init?(json: Any) {
        guard let jsonObject = json as? [String: Any] else {
            return nil
        }

        let identifier = jsonObject["id"] as? Int
        let title = jsonObject["name"] as? String

        let parts: [Movie] = {
            guard let moviesJSON = jsonObject["parts"] as? [[String: Any]] else {
                return []
            }

            let moviesByMappingFromJSON = moviesJSON.map { try? Movie(json: $0) }
            return moviesByMappingFromJSON.flatMap { $0 }
        }()

        self.init(identifier: identifier, title: title, parts: parts)
    }
}

import UIKit

class MovieDetailViewController: UIViewController {
    fileprivate var containerView: MovieDetailContainerView?

    fileprivate var detailView: MovieDetailView? {
        get {
            return containerView?.detailView
        }
        set {
            containerView?.detailView = newValue
        }
    }

    let movieID: Int
    private let movieService = MovieService(session: .shared)
    private let movieCollectionService = MovieCollectionService(session: .shared)
    private let imageService = ImageService(session: .shared)

    private var movieViewModel: MovieViewModel? {
        didSet {
            updateTitle()
            updateDetailView(viewModel: movieViewModel)
        }
    }

    fileprivate var movieCollectionViewModel: MovieCollectionViewModel? {
        didSet {
            updateDetailView(forMovieCollectionViewModel: movieCollectionViewModel)
        }
    }

    init(movieID: Int) {
        self.movieID = movieID
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }

    override func loadView() {
        containerView = MovieDetailContainerView(frame: .zero)
        view = containerView
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        fetchMovie()
    }

    private func fetchMovie() {
        guard movieViewModel == nil else {
            return
        }
        
        movieService.fetchMovie(id: movieID) { [weak self] (movie: Movie?) in
            guard let strongSelf = self else {
                return
            }

            guard let movie = movie else {
                return
            }

            strongSelf.detailView = strongSelf.makeDetailView()
            strongSelf.movieViewModel = MovieViewModel(movie: movie)
            strongSelf.fetchMovieCollectionIfNeeded(identifier: movie.collection?.identifier)
        }
    }

    private func makeDetailView() -> MovieDetailView {
        let detailView = MovieDetailView(frame: .zero,
                                         collectionViewDataSource: self,
                                         collectionViewDelegate: self)
        detailView.collectionView.register(MovieCell.self, forCellWithReuseIdentifier: "Movie")
        return detailView
    }

    private func updateTitle() {
        title = movieViewModel?.title
    }

    private func updateDetailView(viewModel: MovieViewModel?) {
        guard let detailView = detailView else {
            return
        }

        guard let viewModel = viewModel else {
            detailView.image = nil
            detailView.title = nil
            detailView.releaseYear = nil
            detailView.overview = nil
            return
        }

        detailView.title = viewModel.title
        detailView.releaseYear = viewModel.releaseYear
        detailView.overview = viewModel.overview
        detailView.image = nil

        fetchPosterImage(for: viewModel)
    }

    private func updateDetailView(forMovieCollectionViewModel viewModel: MovieCollectionViewModel?) {
        let hidden = (viewModel == nil)
        detailView?.showOrHideCollectionViewAndHeadingLabel(collectionViewHidden: hidden)
        detailView?.collectionView.reloadData()
    }

    private func fetchMovieCollectionIfNeeded(identifier: Int?) {
        guard let identifier = identifier else {
            return
        }

        movieCollectionService.fetchMovieCollection(id: identifier) { [weak self] (collection: MovieCollection?) in
            guard let strongSelf = self else {
                return
            }

            guard let collection = collection else {
                return
            }

            strongSelf.movieCollectionViewModel = MovieCollectionViewModel(collection: collection)
        }
    }

    private func fetchPosterImage(for viewModel: MovieViewModel) {
        let imageWidth = view.bounds.size.width
        let imageWidthByApplyingScreenScale = imageWidth * UIScreen.main.scale
        let roundedImageWidth = Int(imageWidthByApplyingScreenScale.rounded(.up))

        guard let imageURL = viewModel.imageURL(for: roundedImageWidth) else {
            return
        }

        imageService.fetchImage(url: imageURL) { [weak self] (image: UIImage?) in
            guard let strongSelf = self else {
                return
            }

            strongSelf.detailView?.image = image
        }
    }

    fileprivate func fetchImage(forCell cell: MovieCell,
                                indexPath: IndexPath,
                                inCollectionView collectionView: UICollectionView,
                                movieViewModel: MovieViewModel) {

        let cellSize = self.collectionView(collectionView,
                                           layout: collectionView.collectionViewLayout,
                                           sizeForItemAt: indexPath)
        let imageWidthAsFloat = MovieCell.imageWidth(in: cellSize.width)
        let imageWidthByApplyingScale = imageWidthAsFloat * UIScreen.main.scale
        let roundedImageWidth = Int(imageWidthByApplyingScale.rounded(.up))

        guard let imageURL = movieViewModel.imageURL(for: roundedImageWidth) else {
            return
        }

        imageService.fetchImage(url: imageURL) { [weak cell] (image: UIImage?) in
            guard let strongCell = cell else {
                return
            }

            strongCell.image = image
        }
    }
}

extension MovieDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movieCollectionViewModel?.numberOfParts ?? 0
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Movie",
                                                      for: indexPath)
        if let movieCell = cell as? MovieCell {
            configure(movieCell: movieCell, for: indexPath, in: collectionView)
        }
        return cell
    }

    private func configure(movieCell: MovieCell, for indexPath: IndexPath, in collectionView: UICollectionView) {
        guard let movie = movieCollectionViewModel?.part(indexPath: indexPath) else {
            return
        }

        movieCell.title = movie.title
        movieCell.image = nil

        fetchImage(forCell: movieCell,
                   indexPath: indexPath,
                   inCollectionView: collectionView,
                   movieViewModel: movie)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedMovieViewModel = movieCollectionViewModel?.part(indexPath: indexPath) else {
            return
        }

        guard let selectedMovieIdentifier = selectedMovieViewModel.movie.identifier else {
            return
        }

        let detailViewController = MovieDetailViewController(movieID: selectedMovieIdentifier)
        show(detailViewController, sender: nil)
    }
}

extension MovieDetailViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {

        guard let size = detailView?.collectionViewCellSize else {
            return .zero
        }
        return size
    }
}

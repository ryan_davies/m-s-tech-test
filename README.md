# M&S Tech Test

To change the API key, see `ServiceConfiguration`.

## Features

* Displays a grid of films, with images
* Pagination of Now Playing list
* Tap film item to show film details
* Film details includes a larger version of the film image
* Film details includes films that are part of the same collection, if available

## User Interface

On launch, shows a navigation controller with the 'Now Playing' grid of movies.

Tapping a movie pushes a new screen to the navigation stack, showing:

* Film image
* Film name
* Film release year
* Film overview
* If the film is part of a collection, a horizontal carousel of other films in the same collection

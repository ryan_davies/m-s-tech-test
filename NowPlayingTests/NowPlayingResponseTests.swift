import XCTest
@testable import NowPlaying

class NowPlayingResponseTests: XCTestCase {
    func test_MappingFromJSON_CreatesCorrectNumberOfMovies() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.nowPlayingJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            guard let nowPlayingResponse = NowPlayingResponse(json: json) else {
                XCTFail()
                return
            }

            XCTAssertEqual(nowPlayingResponse.movies.count, 20)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
}

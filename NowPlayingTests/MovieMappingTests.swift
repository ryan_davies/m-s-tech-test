import XCTest
@testable import NowPlaying

class MovieMappingTests: XCTestCase {
    func test_MappingMovieFromJSON_SetsIdentifier() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            XCTAssertEqual(movie.identifier, 218)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingMovieFromJSON_SetsTitle() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            XCTAssertEqual(movie.title, "The Terminator")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingMovieFromJSON_SetsReleaseDate() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            let calendar = Calendar(identifier: .gregorian)
            let components = DateComponents(calendar: calendar,
                                            timeZone: TimeZone(secondsFromGMT: 0),
                                            year: 1984,
                                            month: 10,
                                            day: 26)
            let expectedDate = calendar.date(from: components)

            XCTAssertEqual(movie.releaseDate, expectedDate)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingMovieFromJSON_SetsOverview() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            let expectedOverview = "In the post-apocalyptic future, reigning tyrannical supercomputers teleport "
                + "a cyborg assassin known as the \"Terminator\" back to 1984 to kill Sarah Connor, whose unborn son "
                + "is destined to lead insurgents against 21st century mechanical hegemony. Meanwhile, the "
                + "human-resistance movement dispatches a lone warrior to safeguard Sarah. Can he stop the virtually "
                + "indestructible killing machine?"
            XCTAssertEqual(movie.overview, expectedOverview)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingMovieFromJSON_SetsPosterImagePath() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            guard let posterImage = movie.posterImage else {
                XCTFail()
                return
            }

            XCTAssertEqual(posterImage.path, "/q8ffBuxQlYOHrvPniLgCbmKK4Lv.jpg")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingMovieFromJSON_SetsCollectionIdentifier() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])
            let movie = try Movie(json: json)

            guard let collection = movie.collection else {
                XCTFail()
                return
            }

            XCTAssertEqual(collection.identifier, 528)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
}

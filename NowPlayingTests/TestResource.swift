import Foundation

enum TestResource {
    case movieJSON
    case nowPlayingJSON
    case movieCollectionJSON

    enum Error: Swift.Error {
        case resourceDoesNotExist
    }

    var name: String {
        switch self {
        case .movieJSON:
            return "Movie"
        case .nowPlayingJSON:
            return "NowPlaying"
        case .movieCollectionJSON:
            return "MovieCollection"
        }
    }

    var fileExtension: String {
        return "json"
    }

    func data(in bundle: Bundle) throws -> Data {
        guard let url = bundle.url(forResource: name, withExtension: fileExtension) else {
            throw Error.resourceDoesNotExist
        }
        return try Data(contentsOf: url)
    }
}

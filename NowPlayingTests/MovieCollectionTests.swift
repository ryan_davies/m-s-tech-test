import XCTest
@testable import NowPlaying

class MovieCollectionTests: XCTestCase {
    func test_MappingFromJSON_SetsIdentifier() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieCollectionJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            guard let collection = MovieCollection(json: json) else {
                XCTFail()
                return
            }

            XCTAssertEqual(collection.identifier, 528)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingFromJSON_SetsTitle() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieCollectionJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            guard let collection = MovieCollection(json: json) else {
                XCTFail()
                return
            }

            XCTAssertEqual(collection.title, "The Terminator Collection")
        } catch {
            XCTFail(error.localizedDescription)
        }
    }

    func test_MappingFromJSON_CreatesCorrectNumberOfMovies() {
        do {
            let bundle = Bundle(for: type(of: self))
            let data = try TestResource.movieCollectionJSON.data(in: bundle)
            let json = try JSONSerialization.jsonObject(with: data, options: [])

            guard let collection = MovieCollection(json: json) else {
                XCTFail()
                return
            }

            XCTAssertEqual(collection.parts.count, 6)
        } catch {
            XCTFail(error.localizedDescription)
        }
    }
}
